import axios from 'axios'
import config from '../config.js'

export default axios.create({
  baseURL: config.baseUrl,
  headers: {
// 'Content-Type': 'application/x-www-form-urlencoded'
  },
  xsrfCookieName: 'csrftoken',
  xsrfHeaderName: 'X-CSRFToken'
})

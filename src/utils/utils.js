const moment = require('moment')

const utils = {
  pad (num, size) {
    var s = num + ''
    size = size | 2
    while (s.length < size) s = '0' + s
    return s
  },
  duration_str (val) {
    var m = moment.duration(val, 'minutes')
    var str = utils.pad(m.minutes()) + ' минут'
    var hours = m.hours()

    if (hours > 0) {
      var postfix = 'час'

      if (hours >= 2) postfix = 'часа'
      if (hours >= 5) postfix = 'часов'
      str = utils.pad(hours) + ' ' + postfix + ' ' + str
    }

    return str
  },
  id () {
    return '_' + Math.random().toString(36).substr(2, 9)
  }
}

export default utils

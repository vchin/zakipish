import bus from './bus'

let Validator = () => ({
  created () {
    bus.$on('validate', this._validate)
  },
  data () {
    return {
      is_valid: true
    }
  },
  destroyed () {
    this._validate_off()
  },
  methods: {
    _validate () {
      // interface
    },
    _deserialize (data) {
      // interface
    },
    _serialize (data) {
      bus.$emit('serialize', data)
    },
    _raise (data, name = 'error') {
      console.log('raise', name, data)
      bus.$emit(name, data)
    },
    _validate_off () {
      bus.$off('validate', this._validate)
    }
  }

})

export default Validator

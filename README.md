# zakipish

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
## Использование компоненты
[N] - так строка кода обозначается

Запускаем npm run build
В папке dist появится index.html , собственно копируем также стили и js себе в проект
создаем контейнер с **id="app"** и обрабатываем контролы

### Настройки
utils/consts.js - размерность грида и элементов
Например, можно сделать не на 7 дней, а на 14. Сделать элементы шире или уже.

### Работа с данными App.vue
Начальные данные можно запихнуть в [80] data() сразу, или в 
```vuehtml

mounted() {
   $.get(url, (result) => {
      this.data = Object.assign({}, this.data, result)
   })
}
```
ну или на кнопку навесить [153] dataLoad
cохранение см [150] dataSave

### Формат данных 
см [153] dataLoad

```vue
start_date: '2017-11-01 00:00:00',
items: {
  1: {price: 1222, duration: 40, date: '2017-11-01 12:00:00', id: 1},
  2: {price: 8000, duration: 95, date: '2017-11-04 00:15:00', id: 2},
  3: {price: 3000, duration: 44, date: '2017-11-05 22:50:00', id: 3}
}
```

start_date - с какой даты календарь начинает рисоваться. Ахтунг! часы и секунды
в 00:00:00, иначе переколбасит

price - цена, 
duration - продолжительность сеанса в минутах, 
date - старт сеанса
id - id сеанса, если id начинается с подчеркиваниея, то сеанс только создан

